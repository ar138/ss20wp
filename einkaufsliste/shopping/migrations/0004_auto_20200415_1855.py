# Generated by Django 3.0.5 on 2020-04-15 16:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shopping', '0003_item_erledigt'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='menge',
            field=models.TextField(blank=True, default=''),
        ),
    ]

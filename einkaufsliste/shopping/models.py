from django.db import models

# Create your models here.
class Item(models.Model):
    text = models.TextField(blank=False, null=False)
    menge = models.TextField(blank=True, default="")
    erledigt = models.BooleanField(default=False)

def __str__(self):
    erledigt_string = " "
    if self.erledigt:
        erledigt_string ="x"
    return " [{}] {} ({})".format(erledigt_string, self.text, self.menge)
